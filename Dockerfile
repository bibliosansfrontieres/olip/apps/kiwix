# hadolint ignore=DL3007
FROM offlineinternet/olip-base:latest

WORKDIR /tmp/

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
	case $ARCH in \
		armhf) arch='armhf' ;; \
		amd64) arch='x86_64' ;; \
		i386) arch='i586' ;; \
		arm64) arch='armhf' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac && \
	curl -kL https://download.kiwix.org/release/kiwix-tools/kiwix-tools_linux-$arch-3.3.0.tar.gz | tar -xz; \
	mv kiwix-tools*/kiwix-serve kiwix-tools*/kiwix-manage /usr/local/bin; \
	rm -r kiwix-tools*

COPY entrypoint.sh /

COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

COPY scripts/* /usr/local/bin/

EXPOSE 8080

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/usr/bin/supervisord"]
