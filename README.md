# kiwix

[![pipeline status](https://gitlab.com/bibliosansfrontieres/olip/kiwix/badges/master/pipeline.svg)](https://gitlab.com/bibliosansfrontieres/olip/kiwix/commits/master)

The [Kiwix](https://www.kiwix.org/) app for [OLIP](https://gitlab.com/bibliosansfrontieres/olip).

> Kiwix is an offline reader for online content like Wikipedia, Project
> Gutenberg, or TED Talks. It makes knowledge available to people with no or
> limited internet access. The software as well as the content is free to use
> for anyone.

## Docker images

* [offlineinternet/kiwix](https://hub.docker.com/r/offlineinternet/kiwix)
* [registry.gitlab.com/bibliosansfrontieres/olip/apps/kiwix/kiwix:latest](https://gitlab.com/bibliosansfrontieres/olip/apps/kiwix/container_registry/2331097)

## Development

* [Source code](https://gitlab.com/bibliosansfrontieres/olip/kiwix/)
* [Issues](https://gitlab.com/bibliosansfrontieres/olip/kiwix/-/issues)
