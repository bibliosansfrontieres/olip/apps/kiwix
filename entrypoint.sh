#!/bin/bash

# ensure the content folder already exists
mkdir -p /data/content

# Copy an empty library.xml otherwise Kiwix-serve will not start
if [ ! -f "/data/library.xml" ];
then
  echo "<library version=\"20110515\"></library>" > /data/library.xml
fi

exec "$@"
