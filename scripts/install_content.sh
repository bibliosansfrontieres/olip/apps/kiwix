#!/bin/bash

say() {
    >&2 echo "$( date '+%F %T') install_content.sh: $*"
}

say "Installing ${2}..."
/usr/local/bin/kiwix-manage /data/library.xml add "/data/content/$2" \
    && supervisorctl restart kiwix \
    || exit 1
