#!/bin/bash

say() {
    >&2 echo "$( date '+%F %T') uninstall_content.sh: $*"
}

# Print OLIP content_id
echo "$1"
